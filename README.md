# Software Studio 2019 Spring Assignment_02

## Topic
* Project Name : Assignment_02 (Raiden)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animation|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|

## Bonus
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-player(off-line)|5%|N|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|
|Little helper|5%|Y|
|Boss unique movement without attack-mode|?%|Y|

## Website Detail Description

#  Assignment Website�Ghttps://106062271.gitlab.io/Assignment_02

# Jucify mechanisms : 
1. Level:In menu state,we have three start-game button(hard mode, mid mode, easy mode)
2. Skill: The player have one chance to fire large Electromagnetic weapon,and this weapon also is my unique bullet.(bullet with animation)

# Bonus : 
1. Unique bullet: big bullet, many bullet
2. Bullet automatic aiming: The bullets are automatically aiming. They will aim at the player.

