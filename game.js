//state可以是一个自定义对象
var difficult1 = 0;
var difficult2 = 0;
var mylife = 5;
var bosslife = 30;
var kill;
var ult;
var score = 0;
var last_time;
var state1 = {

    preload : function()
    {
        game.load.image('background', 'background.png');        
        game.load.image('play', 'play.png');
    },
    create : function()
    {
        difficult1 = 0;
        difficult2 = 0;
        score = 0;
        map = game.add.tileSprite(0, 0, 800, 600, 'background');
        button = game.add.button(game.world.centerX ,game.world.centerY  , 'play', actionOnClick, this);
        button1 = game.add.button(game.world.centerX ,game.world.centerY+100  , 'play', actionOnClickh, this);
        button2 = game.add.button(game.world.centerX ,game.world.centerY-100  , 'play', actionOnClicke, this);
        easytext = game.add.text(game.world.centerX-400 ,game.world.centerY-125, "easy :" , { font: '50px Arial', fill: '#fff' }); 
        hardtext = game.add.text(game.world.centerX-400 ,game.world.centerY+75, "hard :" , { font: '50px Arial', fill: '#fff' }); 
        midtext = game.add.text(game.world.centerX-400 ,game.world.centerY-25, "mid :" , { font: '50px Arial', fill: '#fff' }); 
        eptext = game.add.text(10, 10, "Z:特殊攻擊(ult為尚能使用次數),space:普通攻擊,方向鍵:移動" , { font: '30px Arial', fill: '#000' });
        button.scale.setTo(0.2,0.2);
        button1.anchor.setTo(0.5,0.5);
        button1.scale.setTo(0.2,0.2);
        button2.anchor.setTo(0.5,0.5);
        button2.scale.setTo(0.2,0.2);
        button.anchor.setTo(0.5,0.5);
        function actionOnClick() {
            difficult1 = 1;
            difficult2 = 20;
            game.state.start("state2");
        }
        function actionOnClickh() {
            difficult1 = 2;
            difficult2 = 40;            
            game.state.start("state2");
        }
        function actionOnClicke() {
            button.kill();
            game.state.start("state2");
        }
    },
    update : function(){ 

    }

}

//state也可以是一个构造函数
var state2 = function(){
        this.preload = function(){
        game.load.image('background', 'background.png'); 
        //game.load.image('myplane', 'myplane.png'); 
        game.load.image('bullet', 'bullet.png');   
        game.load.image('bullet2', 'bullet2.png'); 
        game.load.image('bird', 'bird.png');
        game.load.image('love', 'love.png');
        game.load.image('particle', 'particle.png');
        game.load.spritesheet('myplane', 'myplane.png', 30, 50);
        mylife = 5;
        kill = 0;
    };
    this.create = function(){        
        score = 0;
        last_time = 0; 
        map = game.add.tileSprite(0, 0, 800, 600, 'background');       
        player = game.add.sprite(400, 500, 'myplane');
		player.animations.add('rightwalk', [5, 6, 7, 8], 8, true);
        player.animations.add('leftwalk', [1, 2, 3, 4], 8, true);
        player.animations.add('mid', [0], 8, true);
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);    
        lifetext = game.add.text(10, 10, "life :" + mylife, { font: '50px Arial', fill: '#fff' });    
        ult = 3;
        ulttext = game.add.text(250, 10, "ult:"+ult, { font: '50px Arial', fill: '#fff' });
        scoretext = game.add.text(10, game.height-100, "score:"+score, { font: '50px Arial', fill: '#fff' }); 
        pausetext = game.add.text(game.width/2-300, game.height/2, "PAUSE-press p to continue", { font: '50px Arial', fill: '#fff' });
        pausetext.visible = false;             
        weapon = game.add.weapon(3*3, 'bullet2');
        weapon.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
        weapon.bulletSpeed = 400;
        weapon.fireRate = 250;
        weapon.multiFire = true;
        cursors = this.input.keyboard.createCursorKeys();
        specialfire = this.input.keyboard.addKey(Phaser.KeyCode.Z);
        pause_key = this.input.keyboard.addKey(Phaser.KeyCode.P);
        bulletPositions = [
            { x: 32, y: 0 },
            { x: -32, y: 0 },
            { x: 0, y: 0 },
        ];                
        bulletPool = game.add.weapon(100,'bullet');
        bulletPool.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        bulletPool.bulletSpeed = 1000;
        bulletPool.fireRate = 500;
        bulletPool.fireAngle= 270;
        bulletPool.enableBody = true;
        bulletPool.physicsBodyType = Phaser.Physics.ARCADE;
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
   
        Enemies = game.add.group();
        Enemies.enableBody = true;
        Enemies.physicsBodyType = Phaser.Physics.ARCADE;

        love = game.add.group();
        love.enableBody = true;
        love.physicsBodyType = Phaser.Physics.ARCADE;
        
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.stage.backgroundColor = 0x337799;

        emitter = game.add.emitter(0, 0, 100);

        emitter.makeParticles('particle');
        emitter.gravity = 200;
        /*
        Enemies.createMultiple(5, 'bird');
        Enemies.setAll('anchor.x', 0.5);
        Enemies.setAll('anchor.y', 1);
        Enemies.setAll('scale.x', 50);
        Enemies.setAll('scale.y', 40);
        Enemies.setAll('velocity.x', 400);*/
        //Enemies.forEachExists.kill();
        game.time.events.repeat(6000, 10+difficult1, generateEnemy, this);
        game.time.events.loop(3000, generateV, this);
        function generateV(){
            Enemies.forEach(function(enemy){
                birdvelocityx = game.rnd.between(-game.width/3, game.width/3);
                birdvelocityy = game.rnd.between(-game.height/3, game.height/3);
                enemy.body.velocity.x = birdvelocityx;    
                enemy.body.velocity.y = birdvelocityy; 
            });        
        }        
        function generateEnemy() {
            /*var e = this.enemys.getFirstExists(false);
            if(e) {
              e.reset(game.rnd.integerInRange(0, this.maxWidth), -game.cache.getImage(config.selfPic).height);
              e.life = config.life;
              e.body.velocity.y = config.velocity;
            }*/ //5,6,7
            for(var i = 0; i < 5+difficult1 ; i++)
            {                
                bird = game.add.sprite(game.rnd.between(0, game.width), 0, "bird");
                bird.scale.x = 0.1;
                bird.scale.y = 0.1;
                Enemies.add(bird);
                birdvelocityx = game.rnd.between(-game.width/2, game.width/2);
                birdvelocityy = game.rnd.between(0, game.height/2);
                bird.body.velocity.x = birdvelocityx;    
                bird.body.velocity.y = birdvelocityy;      
                /*game.time.events.loop(100, generateV, bird);
                function generateV(){
                    this.body.velocity.x = game.rnd.between(-game.width/3, game.width/3);   
                };        */        
            }
            solo = game.add.sprite(game.rnd.between(0, game.width), 0, "love");
            love.add(solo);
            solo.body.velocity.y = game.rnd.between(500, game.height/2);
        }    
    };
    this.update = function(){
        pause_key.onDown.add(function(){
            if(game.paused){
                game.paused = false;
                pausetext.visible = false;
            } else{
                game.paused = true;
                pausetext.visible = true;
            }   
        }, this);                
        function particleBurst(pointer) {
            emitter.x = pointer.x;
            emitter.y = pointer.y;
            emitter.start(true, 2000, null, 10);

        }
        if(kill ==  (5+difficult1)*(10+difficult1) )
        {
            game.state.start("state4");            
        }
        if(mylife == 0)
        {
            game.state.start("state3");            
        }
        //  game.physics.arcade.overlap(Bullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(love, player, lhitmy, null, this);  
        game.physics.arcade.overlap(Enemies, player, hitmy, null, this);    
        function lhitmy(my,l)//單人群組
        {
            if(mylife < 5)
                mylife++;
            lifetext.text = "life :" + mylife;
            l.kill();
        } 
        function hitmy(my,e)//單人群組
        {
            if(mylife > 0)
            {    
                mylife = mylife - 1;
                e.kill();
                kill++;
                score = score + 100;
                scoretext.text = "score:"+score;
            }
            lifetext.text = "life :" + mylife;
            if(mylife == 0)
                my.kill();
        }
        game.physics.arcade.overlap(Enemies, bulletPool.bullets, hitEnemy, null, this);
        game.physics.arcade.overlap(Enemies, weapon.bullets, shitEnemy, null, this);        
        function shitEnemy(e,b)
        {
            //b.kill();
            if(e.alive)
            {
                kill++;
                score = score + 100;
                scoretext.text = "score:"+score;
            }
            e.kill();
        }
        function hitEnemy(e,b)
        {
            b.kill();
            e.kill();
            kill++;
            score = score + 100;
            scoretext.text = "score:"+score;
            particleBurst(e);
        }
        map.tilePosition.y += 2;
        player.body.velocity.setTo(0, 0);
        player.animations.play('mid');
        if(fireButton.isDown)
        {
            bulletPool.trackSprite(player);
            bulletPool.fire();
        }
        if (specialfire.isDown)
        {
            weapon.trackSprite(player);           
            weapon.fireMany(bulletPositions);
            if(ult > 0 && (last_time+250 < game.time.now))
            {
                last_time = game.time.now;
                ult--;
            }
             ulttext.text = "ult:"+ult;
        }        
        if (cursors.left.isDown)
        {
            player.animations.play('leftwalk');
            player.body.velocity.x = -300;
            if (cursors.up.isDown)
            {
                player.body.velocity.y = -300;
            }
            else if (cursors.down.isDown)
            {
                player.body.velocity.y = 300;
            }
        }
        else if (cursors.right.isDown)
        {
            player.animations.play('rightwalk');
            player.body.velocity.x = 300;
            if (cursors.up.isDown)
            {
                player.body.velocity.y = -300;
            }
            else if (cursors.down.isDown)
            {
                player.body.velocity.y = 300;
            }
        }
        if (cursors.up.isDown)
        {
            player.body.velocity.y = -300;
            if (cursors.left.isDown)
            {
                player.animations.play('left');
                player.body.velocity.x = -300;
            }
            else if (cursors.right.isDown)
            {
                player.animations.play('right');
                player.body.velocity.x = 300;
            }
        }
        else if (cursors.down.isDown)
        {
            player.body.velocity.y = 300;
            if (cursors.left.isDown)
            {
                player.animations.play('left');
                player.body.velocity.x = -300;
            }
            else if (cursors.right.isDown)
            {
                player.animations.play('right');
                player.body.velocity.x = 300;
            }
        }
        if (player.x > game.width-15) {
            player.x = game.width-15;
        }
        if(player.y > game.height-25){
            player.y = game.height-25;
        }
        if (player.y < 25) {
            player.y = 25;

        }
        if (player.x < 15) {
            player.x = 15;
        }
        Enemies.forEach(function(enemy){
            if(enemy.y > game.height)
                enemy.y = 0;
            if(enemy.y < 0)
                enemy.y = game.height;                  
            if(enemy.x > game.width)
                enemy.x = 0;
            if(enemy.x < 0)
                enemy.x = game.width;          
        });               
    };
}

//只要存在preload、create、update三个方法中的一个就可以了
var state3 = function(){
    this.preload = function(){
        game.load.image('background2', 'background2.png'); 
    };
    this.create = function(){
        map = game.add.tileSprite(0, 0, 800, 600, 'background2');
        restarttext = game.add.text(200, 300, "Press 'R' to restart ", { font: '50px Arial', fill: '#fff' });
        restart_btn = game.input.keyboard.addKey(Phaser.Keyboard.R);
        Scoretext = game.add.text(200, 45, "Score:" + score, { font: '50px Arial', fill: '#fff' });
    };
    this.update = function(){
        if(restart_btn.isDown)
        {
            game.state.start("state1");
        }
    };
}
var weapon;
var state4 = function(){
    this.preload = function(){
        game.load.spritesheet('myplane', 'myplane.png', 30, 50);
        game.load.image('background', 'background.png'); 
        //game.load.image('myplane', 'myplane.png'); 
        game.load.image('bullet', 'bullet.png');
        game.load.image('bossbird', 'bossbird.png');
        game.load.image('enemy_fire', 'enemy_fire.png');        
        game.load.image('love', 'love.png');      
        game.load.image('egg', 'egg.png');
        game.load.image('particle', 'particle.png');  
        game.load.audio('bgm', 'bgm.mp3');
        game.load.audio('bulletsound', 'bullet.mp3');     
    };
    this.create = function(){
        bgm = game.add.audio('bgm',1,true);
        bgm.play();
        bulletsound = game.add.audio('bulletsound',0.1);
        bulletsound.allowMultiple = true;
        last_time = 0;
        map = game.add.tileSprite(0, 0, 800, 600, 'background');    
        player = game.add.sprite(400, 500, 'myplane');
		player.animations.add('rightwalk', [5, 6, 7, 8], 8, true);
        player.animations.add('leftwalk', [1, 2, 3, 4], 8, true);
        player.animations.add('mid', [0], 8, true);   
        //player = game.add.sprite(400, 500, 'myplane');
        boss = game.add.sprite(400, 80, 'egg');
        player.anchor.setTo(0.5, 0.5);
        boss.anchor.setTo(0.5, 0.5);
        boss.scale.x = 0.33;
        boss.scale.y = 0.33;
        mylife = 5;
        bosslife = 50 + difficult2;
        Enemies = game.add.group();
        Enemies.enableBody = true;
        Enemies.physicsBodyType = Phaser.Physics.ARCADE;
        game.physics.enable(player, Phaser.Physics.ARCADE);  
        game.physics.enable(boss, Phaser.Physics.ARCADE);   
        lifetext = game.add.text(10, 10, "mylife :" + mylife, { font: '50px Arial', fill: '#fff' });     
        lifetext2 = game.add.text(400, 10, "bosslife :" + bosslife, { font: '50px Arial', fill: '#fff' });  
        scoretext = game.add.text(10, game.height-100, "score:"+score, { font: '50px Arial', fill: '#fff' });
        pausetext = game.add.text(game.width/2-300, game.height/2, "PAUSE-press p to continue", { font: '50px Arial', fill: '#fff' });
        pausetext.visible = false;
        ult = 3;
        ulttext = game.add.text(250, 10, "ult:"+ult, { font: '50px Arial', fill: '#fff' });  
        cursors = game.input.keyboard.createCursorKeys();
        bulletPool = game.add.weapon(100,'bullet');
        bulletPool.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        bulletPool.bulletSpeed = 1000;
        bulletPool.fireRate = 500;
        bulletPool.fireAngle= 270;
        bulletPool.enableBody = true;
        bulletPool.physicsBodyType = Phaser.Physics.ARCADE;
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        bossPool = game.add.weapon(1000,'enemy_fire');
        bossPool.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        bossPool.bulletSpeed = 500;
        bossPool.fireRate = 0;
        bossPool.fireAngle= 90;
        bossPool.enableBody = true;
        bossPool.physicsBodyType = Phaser.Physics.ARCADE; 
        weapon = game.add.weapon(27, 'bullet');
        weapon.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
        weapon.bulletSpeed = 400;
        weapon.fireRate = 250;
        weapon.multiFire = true;
        cursors = this.input.keyboard.createCursorKeys();
        pause_key = this.input.keyboard.addKey(Phaser.KeyCode.P);
        specialfire = this.input.keyboard.addKey(Phaser.KeyCode.Z);
        bulletPositions = [
            { x: 32, y: 0 },
            { x: -32, y: 0 },
            { x: 0, y: 0 },
            { x: 64, y: 0 },
            { x: -64, y: 0 },
            { x: 128, y: 0 },
            { x: 96, y: 0 },
            { x: -96, y: 0 },
            { x: -128, y: 0 },
        ];                
        love = game.add.group();
        love.enableBody = true;
        love.physicsBodyType = Phaser.Physics.ARCADE;

        game.time.events.loop(6000, generateEnemy, this);
        game.time.events.loop(1000, hit, this);        
        game.time.events.loop(3000, generateV, this);
        game.time.events.loop(1000, generatebV, this);
        function generateV(){
            Enemies.forEach(function(enemy){
                birdvelocityx = game.rnd.between(-game.width/3, game.width/3);
                birdvelocityy = game.rnd.between(-game.height/3, game.height/3);
                enemy.body.velocity.x = birdvelocityx;    
                enemy.body.velocity.y = birdvelocityy; 
             });
       
        }  
        function generatebV(){
            boss.body.velocity.x = game.rnd.between(-game.width/10, game.width/10); 
        }

        function hit()
        {
            Enemies.forEach(function(enemy){
                if(enemy.alive)
                {
                    angle = game.physics.arcade.moveToObject(enemy, player, 0);
                    bossPool.fireAngle= game.math.radToDeg(angle);
                    bossPool.trackSprite(enemy);
                    bossPool.fire();
                }
            });  
        }              
        function generateEnemy() {
            /*var e = this.enemys.getFirstExists(false);
            if(e) {
              e.reset(game.rnd.integerInRange(0, this.maxWidth), -game.cache.getImage(config.selfPic).height);
              e.life = config.life;
              e.body.velocity.y = config.velocity;
            }*/ //5,6,7
            for(var i = 0; i < 3+difficult1 ; i++)
            {                
                bird = game.add.sprite(game.rnd.between(0, game.width), 0, "bossbird");
                bird.scale.x = 0.3;
                bird.scale.y = 0.3;
                Enemies.add(bird);
                birdvelocityx = game.rnd.between(-game.width/2, game.width/2);
                birdvelocityy = game.rnd.between(0, game.height/2);
                bird.body.velocity.x = birdvelocityx;    
                bird.body.velocity.y = birdvelocityy;      
                /*game.time.events.loop(100, generateV, bird);
                function generateV(){
                    this.body.velocity.x = game.rnd.between(-game.width/3, game.width/3);   
                };        */        
            }
            solo = game.add.sprite(game.rnd.between(0, game.width), 0, "love");
            love.add(solo);
            solo.body.velocity.y = game.rnd.between(500, game.height/2);
        }            
    };
    this.update = function(){
        pause_key.onDown.add(function(){
            if(game.paused){
                game.paused = false;
                pausetext.visible = false;
            } else{
                game.paused = true;
                pausetext.visible = true;
            }   
        }, this);        
        if(mylife <= 0)
        {
            bgm.stop();
            game.state.start("state3");            
        }    
        if(bosslife <= 0)
        {
            bgm.stop();
            game.state.start("state5");            
        }            
        //  game.physics.arcade.overlap(Bullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(love, player, lhitmy, null, this);  
        game.physics.arcade.overlap(Enemies, player, hitmy, null, this);    
        game.physics.arcade.overlap(bossPool.bullets, player, bhitmy, null, this);  
        function lhitmy(my,l)//單人群組
        {
            if(mylife < 5)
                mylife++;
            lifetext.text = "life :" + mylife;
            l.kill();
        } 
        function bhitmy(my,e)//單人群組
        {
            if(mylife > 0)
            {    
                mylife = mylife - 1;
                e.kill();
            }
            lifetext.text = "life :" + mylife;
            if(mylife == 0)
                my.kill();
        }
        function hitmy(my,e)//單人群組
        {
            if(mylife > 0)
            {    
                mylife = mylife - 1;
                e.kill();
                kill++;
                score = score + 100;
                scoretext.text = "score:"+score;
            }
            lifetext.text = "life :" + mylife;
            if(mylife == 0)
                my.kill();
        }
        game.physics.arcade.overlap(Enemies, bulletPool.bullets, hitEnemy, null, this);
        game.physics.arcade.overlap(Enemies, weapon.bullets, shitEnemy, null, this);        
        function shitEnemy(e,b)
        {
            b.kill();
            if(e.alive)
            {
                score = score + 100;
                scoretext.text = "score:"+score;
            }
            e.kill();
        }
        function hitEnemy(e,b)
        {
            b.kill();
            score = score + 100;
            scoretext.text = "score:"+score;
            e.kill();
        }
        game.physics.arcade.overlap(boss, bulletPool.bullets, hitboss, null, this);
        game.physics.arcade.overlap(boss, weapon.bullets, shitboss, null, this);        
        function shitboss(e,b)
        {
            bosslife--;
            b.kill();
            lifetext2.text = "bosslife :" + bosslife;    
            score = score + 100;
            scoretext.text = "score:"+score;
        }
        function hitboss(e,b)
        {
            bosslife--;
            b.kill();
            lifetext2.text = "bosslife :" + bosslife; 
            score = score + 100;
            scoretext.text = "score:"+score;
        }        
        map.tilePosition.y += 2;
        player.body.velocity.setTo(0, 0);
        player.animations.play('mid');
        if (specialfire.isDown && ult > 0)
        {
            weapon.trackSprite(player);           
            weapon.fireMany(bulletPositions);
            if(ult > 0 && (last_time+250 < game.time.now))
            {
                last_time = game.time.now;
                ult--;
            }
             ulttext.text = "ult:"+ult;
        }        
        if(fireButton.isDown )
        {
            bulletsound.play();
            bulletPool.trackSprite(player);
            bulletPool.fire();
        }
      /*  if(bigfire.isDown)
        {
            bigfire.trackSprite(player);
            bigfire.fire();
        }*/        
        if (cursors.left.isDown)
        {
            player.animations.play('leftwalk');
            player.body.velocity.x = -300;
            if (cursors.up.isDown)
            {
                player.body.velocity.y = -300;
            }
            else if (cursors.down.isDown)
            {
                player.body.velocity.y = 300;
            }
        }
        else if (cursors.right.isDown)
        {
            player.animations.play('rightwalk');
            player.body.velocity.x = 300;
            if (cursors.up.isDown)
            {
                player.body.velocity.y = -300;
            }
            else if (cursors.down.isDown)
            {
                player.body.velocity.y = 300;
            }
        }
        if (cursors.up.isDown)
        {
            player.body.velocity.y = -300;
            if (cursors.left.isDown)
            {
                player.animations.play('left');
                player.body.velocity.x = -300;
            }
            else if (cursors.right.isDown)
            {
                player.animations.play('right');
                player.body.velocity.x = 300;
            }
        }
        else if (cursors.down.isDown)
        {
            player.body.velocity.y = 300;
            if (cursors.left.isDown)
            {
                player.animations.play('left');
                player.body.velocity.x = -300;
            }
            else if (cursors.right.isDown)
            {
                player.animations.play('right');
                player.body.velocity.x = 300;
            }
        }
        if (player.x > game.width-15) {
            player.x = game.width-15;
        }
        if(player.y > game.height-25){
            player.y = game.height-25;
        }
        if (player.y < 25) {
            player.y = 25;

        }
        if (player.x < 15) {
            player.x = 15;
        }    
    
        if (boss.x < 50) {
            boss.x = 50;
        }      
        if (boss.x > game.width-50) {
            boss.x = game.width-50;
        }
        Enemies.forEach(function(enemy){
            if (enemy.x > game.width-35) {
                enemy.x = game.width-35;
            }
            if(enemy.y > game.height-35){
                enemy.y = game.height-35;
            }
            if (enemy.y < 35) {
                enemy.y = 35;
    
            }
            if (enemy.x < 35) {
                enemy.x = 35;
            }                
        }); 
     }; //其他方法
}

//当然state里也可以存在其他属性或方法
var state5 = function(){
    this.preload = function(){
        game.load.image('background', 'background.png'); 
     };
    this.create = function(){
        map = game.add.tileSprite(0, 0, 800, 600, 'background');
        wintext = game.add.text(200, 100, "WIN ", { font: '100px Arial', fill: '#fff' });
        restarttext = game.add.text(200, 300, "Press 'R' to restart ", { font: '50px Arial', fill: '#fff' });
        Scoretext = game.add.text(200, 50, "Score:" + score, { font: '50px Arial', fill: '#000' });
        restart_btn = game.input.keyboard.addKey(Phaser.Keyboard.R);
    };
    this.update = function(){
        if(restart_btn.isDown)
        {
            game.state.start("state1");
        }
    };

}

var game = new Phaser.Game(800,600);
game.state.add("state1", state1);
game.state.add("state2", state2);
game.state.add("state3", state3);
game.state.add("state4", state4);
game.state.add("state5", state5);
game.state.start("state1");

